export default {
    Background: {
        DARK: 'rgb(15, 15, 15)',
        GRAY: 'rgb(27, 27, 27)',
        LIGHT_OPACITY_005: 'rgba(255, 255, 255, 0.05)',
        LIGHT_OPACITY_01: 'rgba(255, 255, 255, 0.1)',
    },
    Primary: {
        REGULAR: 'rgb(187, 149, 74)',
        DANGER: 'rgb(237, 24, 59)',
        PURPURE: 'rgb(144, 19, 254)',
    },
    Border: {
        GRAY_OPACITY_02: 'rgba(151,151,151,0.2)',
    }
}