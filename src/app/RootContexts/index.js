import React from 'react';

import DimensionsRootContext from './Dimensions';
import DataStoreRootContext from './DataStore';

export default class RootContexts extends React.PureComponent {


  render() {
    return (
      <DimensionsRootContext>
        <DataStoreRootContext>
          {this.props.children}
        </DataStoreRootContext>
      </DimensionsRootContext>
    );
  }
}
