import React from 'react';
import { StyleSheet ,Dimensions, View } from 'react-native';
import DimensionsContext from '../../contexts/Dimensions';

export default class DimensionsRootContext extends React.PureComponent {
    state = {
      screen: Dimensions.get('screen'),
      window: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
      },
    }

    componentDidMount() {
      Dimensions.addEventListener('change', this.handleDimensionsChange);
    }

    componentWillUnmount() {
      Dimensions.removeEventListener('change', this.handleDimensionsChange);
    }

    handleDimensionsChange = ({ screen }) => {
      this.setState({
        screen,
      });
    }

    onLayout = ({ nativeEvent: { layout: { width, height } } }) => {
      this.setState({
        window: {
          width,
          height
        }
      })
    }

    render() {
      return (
        <DimensionsContext.Provider value={this.state}>
          <View style={styles.container} onLayout={this.onLayout}>
            {this.props.children}
          </View>
        </DimensionsContext.Provider>
      );
    }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1
  }
});