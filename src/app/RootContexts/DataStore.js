import React from 'react';
import DummyDataService from '../../core/DummyDataService';
import DataStoreServiceContext from '../../contexts/DataStoreService';

export default class DataStoreRootContext extends React.PureComponent {
  state = {
    dataService: new DummyDataService(),
  };

  render() {
    return (
      <DataStoreServiceContext.Provider value={this.state.dataService}>
        {this.props.children}
      </DataStoreServiceContext.Provider>
    );
  }
}
