export default {
  $interaction: {
    EventTypes: {
      SHOW_CONTROL: 10,
    },
    subscribe: null,
    unsubscribe: null,

    Controls: null,

    showControl: null,
  },

  // $navigation: {
  //   Routes,
  //   RoutesList,
  //   navigate: null,
  //   navigateBack: null,
  //   replace: null,
  // },
};
