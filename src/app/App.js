import React from 'react';
import { StyleSheet, View } from 'react-native';
import LayerControls from './LayerControls';
import RootContexts from './RootContexts';
import Router from '../navigation/Router';

class App extends React.PureComponent {
  render() {
    return (
      <RootContexts>
        <View style={styles.container}>
          <LayerControls>
            <Router />
          </LayerControls>
        </View>
      </RootContexts>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'black',
  }
});

export default App;