import React from 'react';
import { StyleSheet, View, Animated, Easing } from 'react-native';
import app from './index';
import { CreditsBottomSheet, ExitBottomSheet, CaseRoller } from '../components/layers';

export const Controls = {
  CREDITS_BOTTOM_SHEET: 'credits-bottom-sheet',
  EXIT_BOTTOM_SHEET: 'extit-bottom-sheet',

  CASE_ROLLER_BACKDROP_TINT: 'case-roller-backdrop-tint',
};

const ControlsComponents = {
  [Controls.CREDITS_BOTTOM_SHEET]: CreditsBottomSheet,
  [Controls.EXIT_BOTTOM_SHEET]: ExitBottomSheet,
  [Controls.CASE_ROLLER_BACKDROP_TINT]: CaseRoller,
};

class LayerOfControls extends React.Component {
    state = { childStack: [] }

    childrenTransitionAV = new Animated.Value(0);

    childrenWrapperStyle = {
      flexGrow: 1,
      opacity: this.childrenTransitionAV.interpolate({
        inputRange: [0, 1],
        outputRange: [1, 0.4]
      }),
      transform: [{ scale: this.childrenTransitionAV.interpolate({
          inputRange: [0, 1],
          outputRange: [1, 0.9]
      })}]
    };

    componentDidMount() {
      app.$interaction.subscribe(
        app.$interaction.EventTypes.SHOW_CONTROL,
        this.onRequestControl,
      );
    }

    componentDidUpdate() {
      if (this.haveChildren) {
        Animated.timing(this.childrenTransitionAV, {
          toValue: this.haveActiveChildren ? 1 : 0,
          useNativeDriver: true,
          duration: 500,
          easing: Easing.out(Easing.quad)
        }).start();
      }
    }

    componentWillUnmount() {
      app.$interaction.unsubscribe(
        app.$interaction.EventTypes.SHOW_CONTROL,
        this.onRequestControl,
      );
    }

    get haveChildren() {
      return this.state.childStack.length > 0;
    }

    get haveActiveChildren() {
      return this.state.childStack.reduce((a, e) => a || e.active, false);
    }

    pushChildToStack = (type, payload) => {
      this.setState(state => ({
        childStack: [
          ...state.childStack,
          {
            Component: ControlsComponents[type],
            config: ControlsComponents[type].LayerBehaviorSettings,
            payload,
            active: true,
          },
        ],
      }));
    }

    popChildFromStack = () => {
      this.setState(state => ({
        childStack: state.childStack.slice(0, state.childStack.length - 1),
      }));
    }

    onRequestControl = (type, payload) => {
      this.pushChildToStack(type, payload);
    }

    // assume thas is called by last item in stack
    onChildCloseRequest = () => {
      this.setState(({ childStack }) => {
        childStack[childStack.length - 1].active = false;
        setTimeout(this.popChildFromStack, childStack[childStack.length - 1].config.appearOutDuration);
        return { childStack };
      });
    }

    render() {
      const {
        haveChildren, haveActiveChildren, onChildCloseRequest, childrenWrapperStyle,
        state: { childStack },
        props: { children },
      } = this;

      return (
        <>
          <Animated.View style={childrenWrapperStyle}>
            {children}
          </Animated.View>

          <View
            // allow to pass touches through this view while empty
            style={StyleSheet.absoluteFill}
            pointerEvents={haveChildren ? 'auto' : 'none'}
          >
            {haveChildren && childStack.map(({ Component, config, payload, active }, i) => (
              <View
                key={i}
                style={StyleSheet.absoluteFill}
                // prevents the touch passing to component which in closing state (close animation)
                pointerEvents={haveActiveChildren ? 'auto' : 'none'}
              >

                <Component
                  payload={payload}
                  onCloseRequest={onChildCloseRequest}
                />
              </View>
            ))}
          </View>
        </>
      );
    }
}

export default LayerOfControls;
