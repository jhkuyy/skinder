import React from 'react'
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native'
import { Text } from '../base'
import { Colors } from '../../theme'

const propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.element,
};

const defaultProps = {
    children: null
};

const PageTitle = ({ title, children }) => {
    return (
        <View style={styles.container}>
            <View style={styles.titleBefore} />
            <View style={styles.row}>
                <Text appearance="header:3">{title}</Text>
                <View style={styles.titleChildren}>{children}</View>
            </View>
        </View>
    );
};

PageTitle.propTypes = propTypes;

PageTitle.defaultProps = defaultProps;

const styles = StyleSheet.create({
    container: { flexDirection: 'row', alignItems: 'center', marginBottom: 15 },
    titleBefore: {
        width: 3,
        height: 33,
        backgroundColor: Colors.Primary.REGULAR,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
    },
    titleChildren: {
        flexDirection: 'row',
        marginLeft: 'auto',
        paddingRight: 5
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'flex-end',
        lineHeight: 1,
        paddingHorizontal: 15
    }
});

export default PageTitle;