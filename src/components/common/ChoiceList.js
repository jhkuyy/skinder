import React, { Component } from 'react'
import { StyleSheet, Image, View, Dimensions, TouchableOpacity } from 'react-native'
import { Card, Text } from '../base'
import { Colors } from '../../theme'
import checkIcon from '../../../assets/images/icons/check/ico-check.png'

export default class ChoiceList extends Component {

  state = {
    activeItem: null,
  }

  onItemPress = (activeItem) => {
    this.setState({
      activeItem
    });
  }

  renderBlock(item, color, onItemPress) {
    const isActive = item.id === this.state.activeItem;

    const blockStyle = {
      ...styles.block,
      borderColor: isActive ? color : Colors.Background.LIGHT_OPACITY_005
    };

    const blockCheckStyle = {
      ...styles.blockCheck,
      backgroundColor: color
    }

    const check = isActive ?
      <View style={blockCheckStyle}>
        <Image source={checkIcon} style={{width: 15, height: 15}} resizeMode="contain" />
      </View> :
      null;

    return (
      <TouchableOpacity style={styles.blockWrap} onPress={() => onItemPress(item.id)}  key={item.id}>
        <Card style={blockStyle} radius={16} color={Colors.Background.LIGHT_OPACITY_005}>
          <View style={{padding: 15}}>
            {check}
            <Text style={[styles.textPopular, {color}]} fontStyle="text:bold">{item.isPopular ? 'Популярное' : ''}</Text>
            <Text appearance="header:3" align="center">{item.count}</Text>
            <Text style={styles.textCountType} appearance="caption" align="center">{item.countType}</Text>

            <View style={styles.priceRow}>
              <Text style={styles.oldPrice} fontStyle="text:bold">{item.oldPrice}</Text>
              <Text style={{...styles.oldPrice, color}} fontStyle="text:bold">{item.newPrice}</Text>
            </View>
          </View>
        </Card>
      </TouchableOpacity>
    );
  }

  render() {
    const { items, accentColor } = this.props;

    return (
      <View style={styles.container}>
        {items.map(item => this.renderBlock(item, accentColor, this.onItemPress))}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between'
  },
  blockWrap: {
    aspectRatio: 1,
    flexBasis: (Dimensions.get('window').width / 2) - 22.5,
    marginBottom: 15,
  },
  block: {
     borderWidth: 2,
  },
  blockCheck: {
    position: 'absolute',
    right: -1,
    top: -1,
    padding: 7,
    borderTopRightRadius: 16,
    borderBottomLeftRadius: 16
  },
  priceRow: {
     flexDirection: 'row',
     justifyContent: 'center'
  },
  textPopular: {
     textTransform: 'uppercase',
     textAlign: 'center',
     fontSize: 11,
     letterSpacing: 2,
     marginBottom: 14
  },
  textCountType: {
    marginBottom: 25
  },
  oldPrice: {
    fontSize: 13,
    textDecorationLine: 'line-through',
    marginRight: 5
  },
  newPrice: {
    fontSize: 15,
  }
});