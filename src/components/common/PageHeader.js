import React from 'react'
import PropTypes from 'prop-types'
import { withNavigation } from 'react-navigation';
import { StyleSheet, View } from 'react-native'
import { Button } from '../base'
import Icons from '../base/Icon/icons'

const propTypes = {};

const defaultProps = {};

class PageHeader extends React.PureComponent {
  render() {
    const { children, navigation } = this.props;

    return (
      <View style={styles.container}>
         <Button icon={Icons.arrowRight} onPress={() => navigation.goBack()} />
         <View style={styles.content}>
            {children}
         </View>
      </View>
    );
  }
}

PageHeader.propTypes = propTypes;
PageHeader.defaultProps = defaultProps;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(21, 21, 21)',
    flexDirection: 'row',
    alignItems: 'center',
    height: 50
  },
  content: {
    marginLeft: 'auto'
  }
});

export default withNavigation(PageHeader);
