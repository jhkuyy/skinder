import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Image, Animated } from 'react-native';
import { Text, Card, Icon } from '../base';
import Icons from '../base/Icon/icons';
import { Colors } from '../../theme';

const propTypes = {
  item: PropTypes.shape({
    itemName: PropTypes.string.isRequired,
    skinName: PropTypes.string.isRequired,
    isSt: PropTypes.bool.isRequired,
    image: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    rarity: PropTypes.oneOf(['epic', 'rare', 'regular']),
  }),
  showUsername: PropTypes.bool,
  isBig: PropTypes.bool,
  isActive: PropTypes.bool,
};

const defaultProps = {
  showUsername: true,
  isBig: false,
  isActive: false,
};

class WeaponCard extends React.PureComponent {
  animation = new Animated.Value(0);

  constructor(props) {
    super(props);

    this.indicatorStyles = this.indicatorStyles.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isActive !== this.props.isActive && this.props.isActive) {
      Animated.spring(this.animation, {
        toValue: 1,
        tension: 200,
      }).start();
    }
  }

  cardPrimaryColor(type) {
    switch (type) {
      case 'epic':
        return Colors.Primary.PURPURE;
      case 'rare':
        return Colors.Primary.REGULAR;
      case 'regular':
        return Colors.Primary.DANGER;
      default:
        return Colors.Background.LIGHT_OPACITY_01;
    }
  }

  indicatorStyles(type) {
    return {
      ...styles.indicator,
      backgroundColor: this.cardPrimaryColor(type),
    }
  }

  get imageWrapStyles() {
    if (this.props.isBig) {
      return {
        paddingVertical: 55,
      }
    }
    return null;
  }

  get imageStyles() {
    if (this.props.isBig) {
      return {
        width:'100%',
        alignSelf: 'center'
      }
    }

    return {
      width: 130,
      height: 130,
      alignSelf: 'center'
    }
  }

  render() {
    const {
      indicatorStyles,
      cardPrimaryColor,
      imageWrapStyles,
      imageStyles,
      animation,
      style,
      props: { item, showUsername, isBig } } = this;

    const indicator = (
      <View style={styles.indicatorWrap}>
        <View style={indicatorStyles(item.rarity)} />
      </View>
    );

    const username = (
      <View style={styles.footer}>
        <Icon style={styles.userIcon} image={Icons.user} size={15} />
        <Text fontStyle="text:medium" style={styles.footerUsername}>{item.username}</Text>
      </View>
    );

    const info = (
      <View style={styles.info}>
        <View style={styles.infoItem}>
          <Text appearance="caption">Износ</Text>
          <Text appearance="header:6">Factory new</Text>
        </View>
        <View style={styles.infoItem}>
          <Text appearance="caption">Редскость</Text>
          <Text style={{marginBottom: 10}} appearance="header:6">Covert</Text>
          <View style={indicatorStyles(item.rarity)} />
        </View>
        <View style={styles.infoItem}>
          <Text appearance="caption">Коллекция</Text>
          <Text appearance="header:6">Horizon</Text>
        </View>
      </View>
    );


    const scaleValue = animation.interpolate({
      inputRange: [0, 1],
      outputRange: [1, 1.1]
    });

    const background = animation.interpolate({
      inputRange: [0, 1],
      outputRange: [Colors.Background.LIGHT_OPACITY_005, Colors.Background.LIGHT_OPACITY_01],
    });

    const cardStyles = {
      transform: [{scale: scaleValue}]
    };

    return (
      <Card style={[style, styles.card, cardStyles]} radius={16} color={background}>
        { showUsername ? indicator : null }

        <View style={[styles.body]}>
          <View>
            <Text appearance="header:6">{item.itemName}</Text>
            <Text appearance="caption">{item.skinName}</Text>
          </View>
          { item.isSt ? <Text appearance="header:6" color={cardPrimaryColor(item.rarity)}>ST</Text> : null}
        </View>

        <View style={[styles.weaponImageWrap, imageWrapStyles]}>
          <Image style={imageStyles} source={item.image} resizeMode="contain" />
        </View>

        { isBig ? info : null }

        {showUsername ? username : !isBig ? indicator : null }
      </Card>
    );
  }
}

WeaponCard.propTypes = propTypes;
WeaponCard.defaultProps = defaultProps;

const styles = StyleSheet.create({
  userIcon: { width: 15, height: 15, opacity: 0.5 },
  info: {flexDirection: 'row', flexWrap: 'wrap', paddingHorizontal: 5, marginBottom: 10},
  infoItem: { padding: 10, },
  card: {
    width: '100%',
    minWidth: 160,
  },
  indicatorWrap: {
    padding: 15,
  },
  indicator: {
    borderRadius: 5,
    height: 5,
    width: '100%',
  },
  body: {
    paddingTop: 17,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  weaponImageWrap: {
    paddingHorizontal: 15,
  },
  footer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.Background.LIGHT_OPACITY_01,
    padding: 15,
  },
  footerUsername: {
    marginLeft: 5,
    opacity: 0.5
  }
});

export default WeaponCard;
