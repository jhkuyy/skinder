import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import {Button, Text} from '../base';
import {Colors} from '../../theme';
import Icons from '../base/Icon/icons';
import app from '../../app';

const propTypes = {};

const defaultProps = {};

class Balance extends React.PureComponent {
  render() {
    const {} = this;

    return (
        <View style={styles.row}>
          <View style={styles.balance}>
            <Text appearance="caption" align="right">Кредиты</Text>
            <Text appearance="header:7" align="right">17243</Text>
          </View>
          <Button color={Colors.Primary.REGULAR} appearance="circle" icon={Icons.plus}  onPress={() => app.$interaction.showControl(app.$interaction.Controls.CREDITS_BOTTOM_SHEET)} />
        </View>
    );
  }
}

Balance.propTypes = propTypes;
Balance.defaultProps = defaultProps;

const styles = StyleSheet.create({
  row: { flexDirection: 'row', alignItems: 'center' },
  balance: { marginRight: 10 },
});

export default Balance;
