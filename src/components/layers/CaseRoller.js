import React from 'react';
import { Animated, FlatList, StyleSheet, View, Image, Modal } from 'react-native';
import BackdropTint from './BackdropTint';
import { WeaponCard } from '../common';
import { Text } from '../base';
import { withDataStoreService } from '../hoc';
import caseImage from '../../../assets/images/weapon/case.png';
import Colors from '../../theme/colors';
import Icons from '../base/Icon/icons';
import Button from '../base/Button/Button';


class CaseRoller extends React.PureComponent {
  flatListRef = null;

  state = {
    isCloseRequested: false,
    caseItems: null,
    isLoading: true,
    selectedItem: null,
  };

  constructor(props) {
    super(props);

    this.spin = this.spin.bind(this);
    this.renderItem = this.renderItem.bind(this);
  }

  componentDidMount() {
    this.props.dataService.getCaseItems()
      .then((data) => {
        this.setState({
          caseItems: data,
          isLoading: false,
        }, this.spin);
      });
  }



  spin() {
    const rollPosition = new Animated.Value(0);

    const id = rollPosition.addListener(({ value }) => {
      this.flatListRef.scrollToIndex({
        index: value,
        animated: true,
        viewPosition: 0.5
      });
    });

    Animated.timing(
      rollPosition,
      {
        duration: 4000,
        toValue: 25,
        useNativeDriver: true,
        viewPosition: 0.5
      },
    ).start(() => {
      rollPosition.removeListener(id);
      setTimeout(() => {
        this.setState({
          selectedItem: 25
        }, () => {
          setTimeout(() => {
            this.props.payload.onSpinEnd();
            setTimeout(() => {
              this.close();
            }, 300);
          }, 500);
        });
      }, 300);

    });
  }

  setRef = (ref) => this.flatListRef = ref;

  close = () => {
    this.setState({
      isCloseRequested: true,
    });
    this.props.onCloseRequest();
  };

  get data() {
    if (this.state.isLoading) {
      return [];
    }

    return [
      ...this.state.caseItems, ...this.state.caseItems, ...this.state.caseItems, ...this.state.caseItems, ...this.state.caseItems, ...this.state.caseItems, ...this.state.caseItems,
      ...this.state.caseItems, ...this.state.caseItems, ...this.state.caseItems, ...this.state.caseItems, ...this.state.caseItems, ...this.state.caseItems, ...this.state.caseItems,
    ];
  }

  renderItem({item, index}) {
      return (
        <View style={styles.flatListItemWrap}>
          <WeaponCard isBig={false} showUsername={false} item={item} isActive={this.state.selectedItem === index}/>
        </View>
      );
  }

  render() {
    const { close, data, setRef, renderItem, state: { isCloseRequested, isLoading } } = this;

    return (
      <BackdropTint isCloseRequested={isCloseRequested} onCloseRequest={close}>
        <View style={styles.container}>
          <View style={styles.headerWrap}>
            <Text appearance="caption" align="center">Открываем</Text>
            <Text appearance="header:5" align="center">Horzon case</Text>
          </View>

          {isLoading ? (
            <View style={styles.imageWrap}>
              <Image source={caseImage} />
            </View>
           ) : (
            <FlatList
              data={data}
              ref={setRef}
              horizontal
              renderItem={renderItem}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
              scrollEnabled={false}
            />
          )}
        </View>
      </BackdropTint>
    );
  }
}

const styles = StyleSheet.create({
  container: { paddingVertical: 20  },
  headerWrap: { marginTop: 77, marginBottom: 65 },
  imageWrap: { alignItems: 'center', padding: 15 },
  flatListItemWrap: { paddingHorizontal: 10, paddingVertical: 15 }
});

const mapMethodsToProps = (dataService) => {
  return { dataService }
};

const CaseRollerWithDataStoreService = withDataStoreService(mapMethodsToProps)(CaseRoller);

CaseRollerWithDataStoreService.LayerBehaviorSettings = BackdropTint.LayerBehaviorSettings;

export default CaseRollerWithDataStoreService;
