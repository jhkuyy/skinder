import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { PageHeader, PageTitle } from '../common';

const propTypes = {
  pageTitle: PropTypes.string,
  pageTitleContent: PropTypes.element,
  showPageHeader: PropTypes.bool,
};

const defaultProps = {
  pageTitle: null,
  pageTitleContent: null,
  showPageHeader: false,
};

class ScreenWrapper extends React.PureComponent {
  render() {
    const {
      children,
      pageTitle,
      pageTitleContent,
      showPageHeader,
      pageHeaderContent} = this.props;

    return (
      <View style={styles.container}>
        { showPageHeader ? <PageHeader>{pageHeaderContent}</PageHeader> : null }
        { pageTitle ? <PageTitle title={pageTitle}>{pageTitleContent}</PageTitle> : null }
        { children }
      </View>
    );
  }
}

ScreenWrapper.propTypes = propTypes;
ScreenWrapper.defaultProps = defaultProps;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(21, 21, 21)',
  }
});

export default ScreenWrapper;
