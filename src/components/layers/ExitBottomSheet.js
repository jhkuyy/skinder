import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import BottomSheet from './BottomSheet';
import { Button, Text } from '../base'
import { Colors } from '../../theme';
import {Routes} from '../../navigation/Routes';

const propTypes = {};

const defaultProps = {};

class ExitBottomSheet extends React.PureComponent {
  state = {
    isCloseRequested: false,
  };

  close = () => {
    this.setState({
      isCloseRequested: true,
    });
    this.props.onCloseRequest();
  }

  render() {
    const {
      close,
      state: { isCloseRequested },
      props: { payload: { navigation } },
    } = this;

    return (
      <BottomSheet isCloseRequested={isCloseRequested} onCloseRequest={close}>
        <View style={styles.header}>
          <Button textColor={Colors.Primary.REGULAR} fontStyle="text:medium" appearance="link" onPress={() => close()}>Отмена</Button>
        </View>

        <Text style={styles.textHeader} appearance="header:1" align="center">Выйти</Text>
        <Text style={styles.caption} align="center">Вы действительно хотите выйти из {'\n'} своего аккаунта?</Text>

        <View style={styles.buttonWrap}>
          <Button onPress={() => navigation.navigate(Routes.Auth.Login) && close()} appearance="primary">Выйти</Button>
        </View>
      </BottomSheet>
    );
  }
}

ExitBottomSheet.propTypes = propTypes;
ExitBottomSheet.defaultProps = defaultProps;
ExitBottomSheet.LayerBehaviorSettings = BottomSheet.LayerBehaviorSettings;

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15
  },
  textHeader: {
    marginBottom: 10
  },
  caption: {
    marginBottom: 40
  },
  buttonWrap: {
    paddingHorizontal: 15,
    marginBottom: 15
  }
});

export default ExitBottomSheet;
