import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Animated, Easing, TouchableWithoutFeedback } from 'react-native';
import { BackHandlerProvider } from '../base';
import { Colors } from '../../theme';
import { DimensionsContext } from '../../contexts';

const propTypes = {
  isCloseRequested: PropTypes.bool.isRequired,
  onCloseRequest: PropTypes.func.isRequired,
};

const defaultProps = {};

const LayerBehaviorSettings = {
  appearOutDuration: 600,
};

class BottomSheet extends React.PureComponent {

  transitionAV = new Animated.Value(0);

  onLayout = ({ nativeEvent: { layout: { height } }}) => {
    Animated.timing(this.transitionAV, {
      toValue: -height,
      useNativeDriver: true,
      duration: 300,
      easing: Easing.out(Easing.quad)
    }).start();
  };

  componentDidUpdate() {
    if (this.props.isCloseRequested) {
      Animated.timing(this.transitionAV, {
        toValue: 0,
        useNativeDriver: true,
        duration: 400,
        easing: Easing.out(Easing.quad)
      }).start();
    }
  };

  render() {
    const { onLayout, transitionAV, props: { onCloseRequest, children } } = this;

    return (
      <>
        <BackHandlerProvider handler={onCloseRequest} />

        <TouchableWithoutFeedback onPress={onCloseRequest}>
          <View style={StyleSheet.absoluteFill} />
        </TouchableWithoutFeedback>

        <DimensionsContext.Consumer>
          {({ window: { height } }) => <View style={{height}} />}
        </DimensionsContext.Consumer>

        <Animated.View style={[styles.card, { transform: [{ translateY: transitionAV }]}]} onLayout={onLayout}>
          <View style={styles.cardHeader}>
            <View style={styles.cardHeaderInner} />
          </View>
          {children}
        </Animated.View>
      </>
    );
  }
}

BottomSheet.propTypes = propTypes;
BottomSheet.defaultProps = defaultProps;
BottomSheet.LayerBehaviorSettings = LayerBehaviorSettings;

const styles = StyleSheet.create({
  card: {backgroundColor: Colors.Background.GRAY,borderTopRightRadius: 16, borderTopLeftRadius: 16 },
  cardHeader: {alignItems: 'center', justifyContent: 'center', height: 20},
  cardHeaderInner: {backgroundColor: Colors.Background.LIGHT_OPACITY_01, width: 35, height: 5, borderRadius: 2.5}
});

export default BottomSheet;
