import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import BottomSheet from './BottomSheet';
import { Text, Card, Button } from '../base'
import { Colors } from '../../theme'
import { ChoiceList } from '../common'

const propTypes = {};

const defaultProps = {};

const ITEMS = [
  {
    id: 1,
    count: 100,
    isPopular: true,
    countType: 'Кредитов',
    oldPrice: '222 ₽',
    newPrice: '160 ₽',
  },
  {
    id: 2,
    count: 1000,
    countType: 'Кредитов',
    oldPrice: '222 ₽',
    newPrice: '160 ₽',
  },
  {
    id: 3,
    count: 5000,
    countType: 'Кредитов',
    oldPrice: '222 ₽',
    newPrice: '160 ₽',
  },
  {
    id: 4,
    count: 8000,
    countType: 'Кредитов',
    oldPrice: '222 ₽',
    newPrice: '160 ₽',
  },
];

class CreditsBottomSheet extends React.PureComponent {
  state = {
    isCloseRequested: false,
  };

  close = () => {
    this.setState({
      isCloseRequested: true,
    });
    this.props.onCloseRequest();
  }

  render() {
    const {
      close,
      state: { isCloseRequested },
    } = this;

    return (
      <BottomSheet isCloseRequested={isCloseRequested} onCloseRequest={close}>
        <View style={styles.balanceWrap}>
          <Text appearance="caption:small" align="right">Баланс</Text>
          <Text appearance="header:7" align="right">17243</Text>
        </View>

        <Text style={styles.header} appearance="header:1" align="center">Кредиты</Text>
        <Text style={styles.headerCaption} align="center">Получай кредиты и открывай {'\n'} еще больше кейсов</Text>

        <View style={styles.container}>
          <Card style={styles.card} radius={16} color={Colors.Background.LIGHT_OPACITY_005}>
            <View style={styles.cardInner}>
              <View>
                <Text appearance="header:3">Бесплатно</Text>
                <Text appearance="caption">Кредиты</Text>
              </View>
              <Button style={{marginLeft: 'auto'}} color={Colors.Primary.REGULAR} appearance="rounded">Получить</Button>
            </View>
          </Card>

          <ChoiceList items={ITEMS} accentColor={Colors.Primary.REGULAR}/>
        </View>
      </BottomSheet>
    );
  }
}

CreditsBottomSheet.propTypes = propTypes;
CreditsBottomSheet.defaultProps = defaultProps;
CreditsBottomSheet.LayerBehaviorSettings = BottomSheet.LayerBehaviorSettings;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15
  },
  balanceWrap: {
    paddingHorizontal: 25,
  },
  header: {
    marginBottom: 10,
  },
  headerCaption: {
    marginBottom: 30,
  },
  card: {
    height: 78,
    marginBottom: 15
  },
  cardInner: {
    padding: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    textTransform: 'uppercase'
  }
});

export default CreditsBottomSheet;
