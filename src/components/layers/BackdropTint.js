import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Animated, Easing, TouchableWithoutFeedback } from 'react-native';
import { BackHandlerProvider } from '../base';
import { Colors } from '../../theme';

const propTypes = {
  isCloseRequested: PropTypes.bool.isRequired,
  onCloseRequest: PropTypes.func.isRequired,
};

const defaultProps = {};

const LayerBehaviorSettings = {
  appearOutDuration: 600,
};

class BackdropTint extends React.PureComponent {
  transitionAV = new Animated.Value(0);

  onLayout = () => {
    Animated.timing(this.transitionAV, {
      toValue: 0.9,
      useNativeDriver: true,
      duration: 300,
      easing: Easing.out(Easing.quad)
    }).start();
  };

  componentDidUpdate() {
    if (this.props.isCloseRequested) {

      Animated.timing(this.transitionAV, {
        toValue: 0,
        useNativeDriver: true,
        duration: 300,
        easing: Easing.out(Easing.quad)
      }).start();
    }
  };

  render() {
    const { onLayout, transitionAV, props: { onCloseRequest, children } } = this;

    return (
      <>
        <BackHandlerProvider handler={onCloseRequest} />

        <Animated.View style={[StyleSheet.absoluteFill, { backgroundColor: 'black', opacity: transitionAV}]} onLayout={onLayout}>
          {children}
        </Animated.View>
      </>
    );
  }
}

BackdropTint.propTypes = propTypes;
BackdropTint.defaultProps = defaultProps;
BackdropTint.LayerBehaviorSettings = LayerBehaviorSettings;

const styles = StyleSheet.create({
});

export default BackdropTint;
