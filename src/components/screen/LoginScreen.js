import React from 'react';
import { ImageBackground, View, StyleSheet } from 'react-native';
import { Text, Button } from '../base';
import { TextAppearances } from '../base/Text/textAppearance';
import {Routes} from '../../navigation/Routes';
import backgroundImage from '../../../assets/images/bg/login-page-bg.png';

class LoginScreen extends React.PureComponent {
  render() {
    const { navigation } = this.props;

     return (
      <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
        <View style={styles.container}>
          <View style={styles.row}>
            <View style={[styles.col, {paddingRight: 7.5}]}>
              <Button onPress={() => navigation.navigate(Routes.INDEX)} appearance="default">VK</Button>
            </View>
            <View style={[styles.col, {paddingLeft: 7.5}]}>
              <Button onPress={() => navigation.navigate(Routes.INDEX)} appearance="default">Facebook</Button>
            </View>
          </View>

          <Button onPress={() => navigation.navigate(Routes.INDEX)} appearance="primary">Steam</Button>

          <Text style={styles.caption} appearance={TextAppearances.CAPTION}>Пожалуйста войдите или зарегистрируйтесь {'\n'} через одну из соц.сетей.</Text>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  container: {
    width: '100%',
    height: '100%',
    flex: 1,
    padding: 25,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(15, 15, 15, .5)'
  },
  row: {
    flexDirection: 'row',
    marginBottom: 15
  },
  col: {
    width: '50%',
  },
  caption: {
    textAlign: 'center',
    marginTop: 30
  }
});

export default LoginScreen;