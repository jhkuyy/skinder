import React, { PureComponent } from 'react'
import { StyleSheet, ScrollView, View, Switch } from 'react-native'
import { ScreenWrapper, PageHeader } from '../../layers'
import { Button, Card, Text } from '../../base'
import Icons from '../../base/Icon/icons'
import { Colors } from '../../../theme'

export default class SettingsScreen extends PureComponent {
  get pageTitleContent() {
      return <Text appearance="caption">ID 9438273</Text>;
  };

  render() {
    const { pageTitleContent } = this;

    return (
      <ScreenWrapper pageTitle="Настройки" pageTitleContent={pageTitleContent} showPageHeader={true}>
        <View style={styles.container}>
          <ScrollView>
            <View style={{paddingHorizontal: 15}}>
              <View style={{marginBottom: 15}}>
                <Card  style={styles.card} tile="bottom" radius={16} color={Colors.Background.LIGHT_OPACITY_005}>
                  <View style={styles.cardRow}>
                    <Text appearance="default:semibold">Trad URL</Text>
                    <Button color="transparent" appearance="link" textColor={Colors.Primary.REGULAR}>Добавить</Button>
                  </View>
                </Card>

                <Card tile="top" radius={16} color={Colors.Background.LIGHT_OPACITY_005}>
                  <View style={styles.cardRow}>
                    <Text appearance="default:semibold">Пуш уведомления</Text>
                    <Switch thumbColor={Colors.Primary.REGULAR} trackColor={{true: 'rgba(255, 255, 255, 0.1)'}} value={true}></Switch>
                   </View>
                </Card>
              </View>

              <View style={{marginBottom: 15}}>
                <Card radius={16} color={Colors.Background.LIGHT_OPACITY_005}>
                  <View style={styles.cardRow}>
                    <Text  appearance="default:semibold">Поддержка</Text>
                    <Button color="transparent" appearance="link" textColor={Colors.Primary.REGULAR}>Связаться</Button>
                  </View>
                </Card>
              </View>

              <View style={{marginBottom: 15}}>
                <Card style={styles.card} tile="bottom" radius={16} color={Colors.Background.LIGHT_OPACITY_005}>
                  <View style={styles.cardRow}>
                    <Text appearance="default:semibold">FAQ</Text>
                    <Button color="transparent" appearance="circle" icon={Icons.angleLeft} width={30} />
                  </View>
                </Card>
                <Card style={styles.card} tile="all" radius={16} color={Colors.Background.LIGHT_OPACITY_005}>
                  <View style={styles.cardRow}>
                    <Text appearance="default:semibold">Private police</Text>
                    <Button color="transparent" appearance="circle" color="transparent" icon={Icons.angleLeft} width={30} />
                  </View>
                </Card>
                <Card style={styles.card} tile="all" radius={16} color={Colors.Background.LIGHT_OPACITY_005}>
                  <View style={styles.cardRow}>
                    <Text appearance="default:semibold">Terms and Agreement</Text>
                    <Button color="transparent" appearance="circle" icon={Icons.angleLeft} width={30} />
                  </View>
                </Card>
                <Card style={styles.card} tile="top" radius={16} color={Colors.Background.LIGHT_OPACITY_005}>
                  <View style={styles.cardRow}>
                    <Text appearance="default:semibold">Оценить приложение</Text>
                    <Button color="transparent" appearance="circle" icon={Icons.angleLeft} width={30} />
                  </View>
                </Card>
              </View>
            </View>
          </ScrollView>
        </View>
      </ScreenWrapper>
    );
  }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    cardRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 20,
    },
    card: {
        marginBottom: 1
    }
});
