import React, { PureComponent } from 'react';
import { StyleSheet, View, Image, FlatList } from 'react-native';
import { ScreenWrapper, PageHeader } from '../../layers';
import { Button, Card, Text } from '../../base';
import Icons from '../../base/Icon/icons';
import { Colors } from '../../../theme';

import avatar from '../../../../assets/images/profile/avatar.png';

const data = [{}, {}, {}, {}, {}, {}, {}, {}, {}];

export default class PromocodeScreen extends PureComponent {
  get pageTitleContent() {
      return <Button appearance="circle" icon={Icons.share} width={30} />;
  };

  renderItem(item) {
    return (
      <Card style={styles.card} radius={16} color={Colors.Background.LIGHT_OPACITY_005}>
        <View style={styles.cardRow}>
          <Image source={avatar} style={styles.cardImage} resizeMode="contain" />
          <View style={{justifyContent: 'center'}}>
            <Text appearance="caption:medium">3 дня назад</Text>
            <Text appearance="header:6">Username</Text>
          </View>
          <View style={styles.cardAmount}>
              <Card contentStyle={styles.cardAmountContent} radius={16} color={Colors.Background.LIGHT_OPACITY_01}>
              <Text fontStyle="text:bold" color={Colors.Primary.REGULAR}>+40</Text>
            </Card>
          </View>
        </View>
      </Card>
    );
  }

  render() {
    const { pageTitleContent, renderItem } = this;

    return (
      <ScreenWrapper pageTitle="Промокод" pageTitleContent={pageTitleContent} showPageHeader={true}>
        <View style={styles.container}>
          <Card style={styles.promocodeCard} radius={16}>
              <Text style={styles.promocode} fontStyle="mono:bold">2G04RF1</Text>
          </Card>

          <Text style={styles.captionText} appearance="caption">Приглашай друзей через промокод и {'\n'} получай бесплатные кредиты</Text>

          <FlatList data={data} renderItem={renderItem}  keyExtractor={item => item.id} />
        </View>
      </ScreenWrapper>
    );
  }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20
    },
    promocodeCard: {
        borderWidth: 1,
        borderColor: Colors.Border.GRAY_OPACITY_02,
        borderStyle: 'dashed',
        padding: 15,
        height: 108
    },
    promocode: {
      fontSize: 48,
      letterSpacing: 13.9,
      textAlign: 'center'
    },
    captionText: {
      textAlign: 'center',
      marginVertical: 15
    },
    card: {
      marginBottom: 15
    },
    cardRow: {
      flexDirection: 'row',
      flexWrap: 'nowrap',
      alignItems: 'center',
      padding: 15
    },
    cardImage: {
      width: 50,
      height: 50,
      marginRight: 15
    },
    cardAmount: {
      marginLeft: 'auto',
    },
    cardAmountContent: {
      paddingVertical: 5,
      paddingHorizontal: 15
    }
});
