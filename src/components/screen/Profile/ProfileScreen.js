import React, { PureComponent } from 'react';
import {
  View,
  ScrollView,
  Image,
  StyleSheet } from 'react-native';
import { Button, Text, Card } from '../../base';
import app from '../../../app';
import { ScreenWrapper } from '../../layers';
import Icons from '../../base/Icon/icons';
import { TextAppearances } from '../../base/Text/textAppearance';
import { Routes } from "../../../navigation/Routes";
import { Colors } from '../../../theme';
import avatar from '../../../../assets/images/profile/avatar.png';
import ruby from '../../../../assets/images/profile/ruby.png';
import coins from '../../../../assets/images/profile/coins.png';

export default class ProfileScreen extends PureComponent {
  render () {
    const { navigation } = this.props;
    return (
    <>
      <ScreenWrapper>
        <ScrollView style={styles.container}>
          <View style={styles.nav}>
            <Button appearance="circle" icon={Icons.settings} width={30} onPress={() => navigation.push('user:settings')}/>
            <Button appearance="circle" icon={Icons.logout} width={30} onPress={() => app.$interaction.showControl(app.$interaction.Controls.EXIT_BOTTOM_SHEET, { navigation })} />
          </View>
          <View style={styles.avatarContainer}>
            <Image source={avatar} style={{width: 145, height: 145}} resizeMode="contain" />
          </View>
          <Text style={styles.textCenter} appearance="header:2">Username</Text>
          <Text style={styles.textCenter} appearance="caption:medium">Вошли через facebook</Text>

          {subscriptionsBlock}

          {balanceBlock}

          <Text style={styles.textCenter} appearance="caption:medium">Приглашай друзей через промокод и {"\n"} получай бесплатные кредиты</Text>

          {promocodeBlock(navigation)}
        </ScrollView>
      </ScreenWrapper>
    </>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Background.DARK,
    paddingHorizontal: 15,
  },
  nav: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 15
  },
  username: {
    fontSize: 33,
    color: '#fff',
    textAlign: 'center',
    marginTop: '2.2%',
  },
  avatarContainer: {
    alignItems: 'center',
    marginTop: '4.3%',
    marginBottom: '2.2%'
  },
  subscriptionsBlock: {
    borderColor: Colors.Primary.DANGER,
    borderWidth: 1,
    padding: 15,
    marginTop: '5%'
  },
  balanceBlock: {
    backgroundColor: Colors.Background.LIGHT_OPACITY_005,
    padding: 15,
    marginTop: 15,
    marginBottom: 15
  },
  balanceBlockRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  promocodeBlock: {
    borderWidth: 1,
    borderColor: 'rgba(151, 151, 151, 0.2)',
    alignSelf: 'center',
    flexDirection: 'column',
    padding: 15,
    marginTop: 15,
  },
  promocodeBlockRow: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  promocode: {
    color: '#fff',
    fontSize: 17,
    letterSpacing: 3,
    textAlign: 'center'
  },
  textCenter: {
    textAlign: 'center'
  }
});

const subscriptionsBlock = (
  <Card radius={16} style={styles.subscriptionsBlock}>
    <View style={{flexDirection: 'row'}}>
      <Image source={ruby} style={{width: 50, height: 50}}></Image>
      <View style={{paddingHorizontal: 16, paddingVertical: 5}}>
        <Text appearance={TextAppearances.CAPTION}>Premium</Text>
        <Text appearance={TextAppearances.HEADER_6}>24 Дня осталось</Text>
      </View>
    </View>
  </Card>
);

const balanceBlock = (
  <Card radius={16} style={styles.balanceBlock}>
    <View style={styles.balanceBlockRow}>
      <Image source={coins} style={{width: 50, height: 50}}></Image>
      <View style={{paddingHorizontal: 16, paddingVertical: 5}}>
        <Text appearance={TextAppearances.CAPTION}>Кредитов</Text>
        <Text appearance={TextAppearances.HEADER_6}>40 123</Text>
      </View>
      <Button style={{marginLeft: 'auto'}} appearance="circle" icon={Icons.plus} width={30}  onPress={() => app.$interaction.showControl(app.$interaction.Controls.CREDITS_BOTTOM_SHEET)} />
    </View>
  </Card>
);

const promocodeBlock = (navigation) => (
  <View style={styles.promocodeBlockRow}>
    <Card radius={16} style={styles.promocodeBlock}>
      <Button appearance="link" onPress={() => navigation.push(Routes.User.PROMOCODE)}>
        <Text fontStyle="mono:bold" style={styles.promocode}>2G04RF1</Text>
      </Button>
    </Card>
  </View>
);