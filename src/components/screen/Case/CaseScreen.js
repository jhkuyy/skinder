import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, FlatList, Image } from 'react-native';
import { Button, Text, Card } from '../../base';
import { WeaponCard } from '../../common';
import { ScreenWrapper } from '../../layers';
import Icons from '../../base/Icon/icons';
import { Colors } from '../../../theme';
import caseImage from '../../../../assets/images/weapon/case.png';
import {Routes} from '../../../navigation/Routes';
import Balance from '../../common/Balance';
import { withDataStoreService } from '../../hoc';

const propTypes = {};

const defaultProps = {};

class CaseScreen extends React.PureComponent {
  state = {
    caseItems: null
  };

  componentDidMount() {
    this.props.dataService.getCaseItems()
      .then((data) => {
        this.setState({
          caseItems: data,
        });
      });
  }

  renderCaseItem = ({ index }) => {
    return (
      <Card style={styles.caseItem} color={Colors.Background.LIGHT_OPACITY_005} contentStyle={styles.caseItemContent} radius={16} key={index}>
        <Image style={styles.caseItemImage} source={caseImage}/>
        <View style={styles.caseItemTextWrap}>
          <Text appearance="header:6">Free case</Text>
          <Text appearance="caption">Выдано 2332</Text>
        </View>
        <View style={styles.caseItemButtonsWrap}>
          <Button
            appearance="rounded"
            color={Colors.Background.LIGHT_OPACITY_01}
            textColor={Colors.Primary.REGULAR}
            onPress={() => this.props.navigation.push(Routes.Case.OPEN)}
          >
            Free
          </Button>
          <Button appearance="circle" color="transparent" icon={Icons.angleLeft} />
        </View>
      </Card>
    );
  };

  render() {
    const { caseItems } = this.state;

    return (
      <ScreenWrapper showBottomNavigation={true}>
        <View style={styles.container}>
          <View style={[styles.row, styles.header]}>
            <Text style={styles.headerText} appearance="header:3">Skinder</Text>
            <Balance />
          </View>

          <Text style={styles.caption} appearance="caption">Последние победы</Text>

          <View style={{paddingHorizontal: 10}}>
            <FlatList
              data={caseItems}
              renderItem={({ item }) => {
                return (
                  <View style={{paddingHorizontal: 5, flex: 1}}>
                    <WeaponCard isBig={false} showUsername={true} item={item}/>
                  </View>
                );
              }}
              showsHorizontalScrollIndicator={false}
              keyExtractor={item => item.id.toString()}
              horizontal={true} />
          </View>

          <View style={styles.caseItemWrap}>
            <FlatList
              data={[{id: 1},{id: 2},{id: 3},{id: 4},{id: 6},{id: 7}]}
              showsVerticalScrollIndicator={false}
              renderItem={this.renderCaseItem}
              keyExtractor={item => item.id.toString()}
            />
          </View>
        </View>
      </ScreenWrapper>
    );
  }
}

CaseScreen.propTypes = propTypes;
CaseScreen.defaultProps = defaultProps;

const styles = StyleSheet.create({
  container: { flex: 1 },
  header: { padding: 15 },
  headerText: {  marginRight: 'auto' },
  row: { flexDirection: 'row', alignItems: 'center' },
  caption: { marginBottom: 15, paddingHorizontal: 15 },
  caseItemWrap: { padding: 15, marginTop: 15, flex: 1, backgroundColor: Colors.Background.DARK },
  caseItem: { marginBottom: 15 },
  caseItemContent: { padding: 15, flexDirection: 'row' },
  caseItemImage: { width: 50, height: 50 },
  caseItemTextWrap: { justifyContent: 'center', marginLeft: 15 },
  caseItemButtonsWrap: { marginLeft: 'auto', alignItems: 'flex-end' },
});

const mapMethodsToProps = (dataService) => {
  return { dataService }
};

export default withDataStoreService(mapMethodsToProps)(CaseScreen);
