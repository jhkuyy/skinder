import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, Image, ScrollView, Modal} from 'react-native';
import { Button, Text, Card } from '../../base';
import { WeaponCard } from '../../common';
import { ScreenWrapper } from '../../layers';
import app from '../../../app';
import { Colors } from '../../../theme';
import caseImage from '../../../../assets/images/weapon/case.png';
import Balance from '../../common/Balance';
import { withDataStoreService } from '../../hoc';
import Icons from '../../base/Icon/icons';


const propTypes = {};

const defaultProps = {};

class CaseOpenScreen extends React.PureComponent {
  state = {
    isCaseItemsLoading: true,
    caseItems: null,
    isModalVisible: false,
  };

  componentDidMount() {
    this.props.dataService.getCaseItems()
      .then((data) => {
        this.setState({
          caseItems: data,
          isCaseItemsLoading: false,
        });
      });
  }

  get pageHeaderContent() {
    return (
      <View style={styles.balanceWrap}>
        <Balance />
      </View>
    );
  }

  closeModal = () => {
    this.setState({
      isModalVisible: false,
    });
  };

  onSpinEnd = () => {
    this.setState({
      isModalVisible: true,
    });
  };

  render() {
    const { pageHeaderContent, closeModal, onSpinEnd, state: { caseItems, isModalVisible, isCaseItemsLoading } } = this;

    return (
      <>
        <ScreenWrapper showPageHeader={true} pageHeaderContent={pageHeaderContent}>
          <ScrollView>
            <View style={styles.container}>
              <View style={styles.imageWrap}>
                <Image style={styles.image} resizeMode="contain" source={caseImage} />
              </View>
              <View style={styles.infoRow}>
                <View>
                  <Text appearance="header:6">Horizon</Text>
                  <Text appearance="caption">Выдано 2832</Text>
                </View>
                <View style={styles.openButtonWrap}>
                  <Card radius={16} tile="right" contentStyle={styles.openButtonBadge} color="#000">
                    <Text>40Р</Text>
                  </Card>
                  <Button
                    tile="left"
                    appearance="rounded"
                    color={Colors.Primary.REGULAR}
                    onPress={() => app.$interaction.showControl(app.$interaction.Controls.CASE_ROLLER_BACKDROP_TINT, { onSpinEnd })}
                  >
                    Открыть
                  </Button>
                </View>
              </View>

              <View style={styles.buttonGroup}>
                <Button style={styles.buttonGroupButton} tile="right" appearance="default">
                  <View style={styles.rowCentered}>
                    <Text style={styles.buttonGroupButtonPercent} appearance="header:6">+10%</Text>
                    <Text appearance="caption:small">15Р</Text>
                  </View>
                </Button>
                <Button style={styles.buttonGroupButton} tile="all" appearance="default">
                  <View style={styles.rowCentered}>
                    <Text style={styles.buttonGroupButtonPercent} appearance="header:6">+20%</Text>
                    <Text appearance="caption:small">25Р</Text>
                  </View>
                </Button>
                <Button style={styles.buttonGroupButtonLast} tile="left" appearance="default">
                  <View style={styles.rowCentered}>
                    <Text style={styles.buttonGroupButtonPercent} appearance="header:6">+30%</Text>
                    <Text appearance="caption:small">55Р</Text>
                  </View>
                </Button>
              </View>
              <Text style={{marginBottom: 10}} appearance="caption" align="center">Увеличитель шанса</Text>
            </View>
            <View style={styles.priceListWrap}>
              <Text style={styles.priceListText} appearance="caption" align="center">Кейс содержит следующие призы</Text>

              <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                {isCaseItemsLoading ? null :
                  caseItems.map((item) => {
                    return (
                      <View style={styles.flatListItem}>
                        <WeaponCard isBig={false} showUsername={true} item={item}/>
                      </View>
                    );
                })}
              </View>
            </View>
          </ScrollView>
        </ScreenWrapper>

        <Modal
          transparent={true}
          visible={isModalVisible}>
          <View style={{padding: 15, flex: 1, backgroundColor: 'rgb(0, 0, 0)'}}>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 25}}>
              <View>
                <Text appearance="header:5">Horizon case</Text>
                <Text appearance="caption">Открыт</Text>
              </View>
              <Button appearance="circle" color={Colors.Background.LIGHT_OPACITY_01} icon={Icons.close} onPress={() => { closeModal(); }}/>
            </View>

            <WeaponCard isBig showUsername={false} item={ isCaseItemsLoading ? null : caseItems[0]} />

            <View style={{marginVertical: 15}}>
              <Text appearance="caption" align="center">Вывести или обменять предмет, вы {'\n'} можете на вкладке инвентрать</Text>
            </View>

            <Button
              appearance="default"
              color={Colors.Primary.REGULAR}
              onPress={() => {
                closeModal();
                setTimeout(() => {
                  app.$interaction.showControl(app.$interaction.Controls.CASE_ROLLER_BACKDROP_TINT, { onSpinEnd });
                }, 200);
              }}
            >
              Открыть еще один за 40р
            </Button>
          </View>
        </Modal>
      </>
    );
  }
}

CaseOpenScreen.propTypes = propTypes;
CaseOpenScreen.defaultProps = defaultProps;

const styles = StyleSheet.create({
  container: { backgroundColor: 'rgb(21, 21, 21)', paddingHorizontal: 25 },
  imageWrap: { alignItems: 'center', padding: 15 },
  image: { width: 270 },
  infoRow: { flexDirection: 'row', alignItems: 'center' },
  openButtonWrap: { marginLeft: 'auto', flexDirection: 'row', flexWrap: 'nowrap', alignItems: 'center' },
  openButtonBadge: { height: 33, paddingHorizontal: 15,justifyContent: 'center' },
  buttonGroup: { flexDirection: 'row', paddingTop: 25, paddingBottom: 10 },
  buttonGroupButton: {marginRight: 1, flexGrow: 1},
  buttonGroupButtonPercent: {marginRight: 3},
  buttonGroupButtonLast: {flexGrow: 1},
  rowCentered: { flexDirection: 'row', alignItems: 'center' },
  balanceWrap: { marginRight: 15 },
  row: { flex: 1, justifyContent: 'space-between', marginBottom: 20  },
  priceListWrap: { backgroundColor: 'rgb(15, 15, 15)', flex: 1, padding: 10 },
  priceListText: { marginBottom: 10 },
  flatListItem: { padding: 5, width: '50%' }
});

const mapMethodsToProps = (dataService) => {
  return { dataService }
};

export default withDataStoreService(mapMethodsToProps)(CaseOpenScreen);
