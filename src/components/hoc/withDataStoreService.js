import React from 'react';
import DataStoreServiceContext from '../../contexts/DataStoreService';

const withDataStoreService = (mapMethodsToProps) => (Wrapped) => {
  return (props) => {
    return (
      <DataStoreServiceContext.Consumer>
        {
          (dataService) => {
            const serviceProps = mapMethodsToProps(dataService);

            return <Wrapped {...props} {...serviceProps} />;
          }
        }
      </DataStoreServiceContext.Consumer>
    )
  };
};

export default withDataStoreService;