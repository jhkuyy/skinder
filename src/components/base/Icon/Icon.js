import React from 'react'
import PropTypes from 'prop-types'
import { Image } from 'react-native'

const propTypes = {
  image: PropTypes.number.isRequired,
  size: PropTypes.number,
};

const defaultProps = {
  size: 15,
};

class Icon extends React.PureComponent {
  render() {
    const { image, size, ...props } = this.props;

    return <Image source={image} width={size} resizeMethod="scale" {...props}/>;
  }
}

Icon.propTypes = propTypes;
Icon.defaultProps = defaultProps;

export default Icon;
