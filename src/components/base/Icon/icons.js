const Icons = {
  angleLeft: require('../../../../assets/images/icons/angle-left/angle-left.png'),
  arrowRight: require('../../../../assets/images/icons/arrow-right/arrow-right.png'),
  check: require('../../../../assets/images/icons/check/ico-check.png'),
  logout: require('../../../../assets/images/icons/logout/ico-logout.png'),
  settings: require('../../../../assets/images/icons/settings/ico-settings.png'),
  plus: require('../../../../assets/images/icons/plus/ico-plus.png'),
  home: require('../../../../assets/images/icons/home/home.png'),
  star: require('../../../../assets/images/icons/star/star.png'),
  ticket: require('../../../../assets/images/icons/ticket/ticket.png'),
  user: require('../../../../assets/images/icons/user/user.png'),
  timeSchedule: require('../../../../assets/images/icons/time-schedule/time-schedule.png'),
  share: require('../../../../assets/images/icons/share/ico-share.png'),
  close: require('../../../../assets/images/icons/close/ico-close.png')
}

export default Icons;