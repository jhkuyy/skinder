import React from 'react';
import PropTypes from 'prop-types';
import { BackHandler } from 'react-native';

const propTypes = {
  handler: PropTypes.func.isRequired,
};

class BackHandlerProvider extends React.Component {
  constructor(props) {
    super(props);

    this.handle = this.handle.bind(this);
  }

  componentDidMount() {
    this.listener = BackHandler.addEventListener('hardwareBackPress', this.handle);
  }

  componentWillUnmount() {
    if (this.listener) this.listener.remove();
  }

  handle() {
    this.props.handler();
    return true;
  }

  render() {
    return null;
  }
}

BackHandlerProvider.propTypes = propTypes;

export default BackHandlerProvider;
