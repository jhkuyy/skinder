import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Touchable, Animated } from 'react-native'
import RoundView from './../RoundView'

const propTypes = {
  tile: PropTypes.oneOf(['all', 'top', 'bottom', 'left', 'right']),
  color: PropTypes.any,
};

const defaultProps = {
  tile: null,
  color: 'transparent',
};

class Card extends React.PureComponent {
  constructor(props) {
      super(props);
      this.Wrapper = props.onPress ? Touchable : Animated.View;
  }

  get contentStyles() {
    const { contentStyle, color } = this.props;

    return [
      contentStyle,
      styles.content,
      {backgroundColor: color}
    ];
  }

  render() {
    const {
        Wrapper,
        contentStyles,
        props: {
          style,
          children,
          radius,
          tile,
          color,
          ...props
        },
    } = this;

    return (
      <RoundView
        style={[style, styles.wrapper]}
        radius={radius}
        tile={tile}
      >
        <Wrapper
          {...props}
          style={contentStyles}
        >
            {children}
        </Wrapper>
      </RoundView>
    );
  }
}

Card.propTypes = propTypes;
Card.defaultProps = defaultProps;

const styles = StyleSheet.create({
  wrapper: { overflow: 'hidden' },
  content: { flexGrow: 1 },
});

export default Card;
