import { StyleSheet } from 'react-native';

export const TextAppearances = {
    DEFAULT: 'default',
    DEFAULT_SEMIBOLD: 'default:semibold',
    HEADER_1: 'header:1',
    HEADER_2: 'header:2',
    HEADER_3: 'header:3',
    HEADER_4: 'header:4',
    HEADER_5: 'header:5',
    HEADER_6: 'header:6',
    HEADER_7: 'header:7',
    CAPTION: 'caption',
    CAPTION_MEDIUM: 'caption:medium',
    CAPTION_SMALL: 'caption:small',
};

export const FontStyles = {
    Display: {
        BOLD: 'display:bold',
        SEMIBOLD: 'display:semibold',
        MEDIUM: 'display:medium',
        REGULAR: 'display:regular',
    },
    Text: {
        BOLD: 'text:bold',
        SEMIBOLD: 'text:semibold',
        MEDIUM: 'text:medium',
        REGULAR: 'text:regular',
    },
    Mono: {
        BOLD: 'mono:bold',
        HEAVY: 'mono:heavy'
    }
};

export const FontStylesFontFamily = {
    Display: {
      BOLD: 'SFProDisplay-Bold',
      SEMIBOLD: 'SFProDisplay-Semibold',
      MEDIUM: 'SFProDisplay-Medium',
      REGULAR: 'SFProDisplay-Regular',
    },
    Text: {
        BOLD: 'SFProText-Bold',
        SEMIBOLD: 'SFProText-Semibold',
        MEDIUM: 'SFProText-Medium',
        REGULAR: 'SFProText-Regular',
    },
    Mono: {
        BOLD: 'SFMono-Bold',
        HEAVY: 'SFMono-Heavy',
    }
};

export const fontStyleStyles = StyleSheet.create({
    [FontStyles.Display.HEAVY]: { fontFamily: FontStylesFontFamily.Display.HEAVY },
    [FontStyles.Display.BOLD]: { fontFamily: FontStylesFontFamily.Display.BOLD },
    [FontStyles.Display.MEDIUM]: { fontFamily: FontStylesFontFamily.Display.MEDIUM },
    [FontStyles.Display.REGULAR]: { fontFamily: FontStylesFontFamily.Display.REGULAR },

    [FontStyles.Text.BOLD]: { fontFamily: FontStylesFontFamily.Text.BOLD },
    [FontStyles.Text.SEMIBOLD]: { fontFamily: FontStylesFontFamily.Text.SEMIBOLD },
    [FontStyles.Text.MEDIUM]: { fontFamily: FontStylesFontFamily.Text.MEDIUM },
    [FontStyles.Text.REGULAR]: { fontFamily: FontStylesFontFamily.Text.REGULAR },

    [FontStyles.Mono.BOLD]: { fontFamily: FontStylesFontFamily.Mono.BOLD },
    [FontStyles.Mono.HEAVY]: { fontFamily: FontStylesFontFamily.Mono.HEAVY },
});

export const styles = StyleSheet.create({
    [TextAppearances.DEFAULT]: { fontSize: 15, fontFamily: FontStylesFontFamily.Text.REGULAR },
    [TextAppearances.DEFAULT_SEMIBOLD]: { fontSize: 15, fontFamily: FontStylesFontFamily.Text.SEMIBOLD },
    [TextAppearances.CAPTION]: { fontSize: 13, fontFamily: FontStylesFontFamily.Text.REGULAR, opacity: 0.2 },
    [TextAppearances.CAPTION_MEDIUM]: { fontSize: 13, fontFamily: FontStylesFontFamily.Text.MEDIUM, opacity: 0.2 },
    [TextAppearances.CAPTION_SMALL]: { fontSize: 11, fontFamily: FontStylesFontFamily.Text.REGULAR, opacity: 0.2 },

    [TextAppearances.HEADER_1]: { fontSize: 33, fontFamily: FontStylesFontFamily.Display.BOLD },
    [TextAppearances.HEADER_2]: { fontSize: 30, fontFamily: FontStylesFontFamily.Text.BOLD },
    [TextAppearances.HEADER_3]: { fontSize: 27, fontFamily: FontStylesFontFamily.Display.BOLD },
    [TextAppearances.HEADER_4]: { fontSize: 22, fontFamily: FontStylesFontFamily.Display.BOLD },
    [TextAppearances.HEADER_5]: { fontSize: 20, fontFamily: FontStylesFontFamily.Display.BOLD },
    [TextAppearances.HEADER_6]: { fontSize: 15, fontFamily: FontStylesFontFamily.Text.BOLD },
    [TextAppearances.HEADER_7]: { fontSize: 13, fontFamily: FontStylesFontFamily.Text.BOLD },
});