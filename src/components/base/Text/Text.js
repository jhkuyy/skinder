import React from 'react'
import PropTypes from 'prop-types'
import { Text as BaseText } from 'react-native'
import { styles, fontStyleStyles } from './textAppearance';

const propTypes = {
    fontStyle: PropTypes.string,
    color: PropTypes.string,
    align: PropTypes.string,
};

const defaultProps = {
    fontStyle: null,
    color: '#fff',
    align: 'left',
};

class Text extends React.PureComponent {
    render() {
      const {
        appearance,
        children,
        fontStyle,
        style,
        color,
        align,
        ...props
      } = this.props;

      return (
        <BaseText {...props}
          style={[
            { color },
            { textAlign: align },
            appearance && styles[appearance],
            fontStyle && fontStyleStyles[fontStyle],
            style,
          ]}
        >
          {children}
        </BaseText>
      );
    }
  }

Text.propTypes = propTypes;
Text.defaultProps = defaultProps;

export default Text;
