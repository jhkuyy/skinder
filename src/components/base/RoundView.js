import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Animated } from 'react-native';

const propTypes = {
    radius: PropTypes.number,
    tile: PropTypes.oneOf(['none', 'all', 'top', 'bottom', 'left', 'right']),
};

const defaultProps = {
    radius: 0,
    tile: 'none',
};

const TileMapping = {
    all: borderRadius => ({ borderRadius }),
    top: r => ({ borderBottomLeftRadius: r, borderBottomRightRadius: r }),
    bottom: r => ({ borderTopLeftRadius: r, borderTopRightRadius: r }),
    left: r => ({ borderTopRightRadius: r, borderBottomRightRadius: r }),
    right: r => ({ borderTopLeftRadius: r, borderBottomLeftRadius: r }),
};

function mapPropToStyle(radius, tile) {
    if (tile === 'all') return null;

    return TileMapping[tile || 'all'](radius || 0);
}

const RoundView = ({ children, style, radius, tile, ...props }) => {
    return (
        <Animated.View {...props} style={[style, styles.base, mapPropToStyle(radius, tile)]}>
            {children}
        </Animated.View>
    );
}

RoundView.propTypes = propTypes;
RoundView.defaultProps = defaultProps;

const styles = StyleSheet.create({
    base: { overflow: 'hidden' },
});

export default RoundView;
