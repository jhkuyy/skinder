import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, TouchableOpacity, View } from 'react-native'
import { FontStyles } from './../../base/Text/textAppearance'
import Icon from './../Icon'
import Card from './../Card'
import Text from './../Text'
import { Colors } from './../../../theme'

const propTypes = {
  color: PropTypes.string,
  textColor: PropTypes.string,
  loading: PropTypes.bool,
  icon: PropTypes.number,
  block: PropTypes.bool,
  touchAvailable: PropTypes.bool,
  radius: PropTypes.number,
};

const defaultProps = {
  color: null,
  textColor: '#fff',
  loading: false,
  icon: null,
  block: false,
  radius: 16,
};

const ButtonAppearances = {
  DEFAULT: 'default',
  PRIMARY: 'primary',
  PRIMARY_DANGER: 'primary:danger',
  LINK: 'link',
  CIRCLE: 'circle',
  ROUNDED: 'rounded',
};

const AppearanceFill = {
  [ButtonAppearances.DEFAULT]: Colors.Background.LIGHT_OPACITY_01,
  [ButtonAppearances.CIRCLE]: Colors.Background.LIGHT_OPACITY_01,
  [ButtonAppearances.PRIMARY]: Colors.Primary.REGULAR,
  [ButtonAppearances.PRIMARY_DANGER]: Colors.Primary.DANGER,
  [ButtonAppearances.LINK]: 'transparent'
};

const ContainerStyles = {
  [ButtonAppearances.DEFAULT]: {height: 50},
  [ButtonAppearances.PRIMARY]: {height: 50},
  [ButtonAppearances.PRIMARY_DANGER]: {height: 50},
  [ButtonAppearances.LINK]: {height: 27},
  [ButtonAppearances.ROUNDED]: {height: 33},
  [ButtonAppearances.CIRCLE]: {height: 30, width: 30},
}

const ContentStyles = {
  [ButtonAppearances.DEFAULT]: {paddingHorizontal: 10},
  [ButtonAppearances.PRIMARY]: {paddingHorizontal: 10},
  [ButtonAppearances.PRIMARY_DANGER]: {paddingHorizontal: 10},
  [ButtonAppearances.LINK]: {paddingHorizontal: 0},
  [ButtonAppearances.CIRCLE]: {padding: 5}
};

class Button extends React.PureComponent {
  get touchAvailable() {
    const { disabled, loading } = this.props;

    return !disabled && !loading;
  }

  render() {
    const {
      touchAvailable,
      props: {
        children,
        style,
        disabled,
        appearance,
        loading,
        icon,
        tile,
        block,
        color,
        textColor,
        radius = 16,
        ...props
      },
    } = this;

    return (
        <TouchableOpacity
          {...props}
          style={[style, block && styles.containerBlock]}
          disabled={!touchAvailable}
        >
          <Card
            style={ContainerStyles[appearance] || styles.container}
            contentStyle={ContentStyles[appearance] || styles.containerContent}
            radius={radius}
            tile={tile}
            color={color || AppearanceFill[appearance]}
          >
            <View style={styles.contentRow}>
              {icon && <Icon style={styles.icon} image={icon} size={18} />}

              {typeof children === 'string' ? (
                <Text color={textColor} fontStyle={FontStyles.Display.SEMIBOLD}>
                  {children}
                </Text>
              ) : children}
            </View>
          </Card>
        </TouchableOpacity>
    );
  }
}

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

const styles = StyleSheet.create({
  container: { height: 50 },
  containerBlock: { width: '100%' },
  containerContent: { paddingHorizontal: 10 },
  contentRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    flex: 1,
  },
});


export default Button;
