const lastOpenedCasePrices = [
  {
    id: 1,
    itemName: 'AK-47',
    skinName: 'Neon Rider',
    isSt: true,
    image: require('../../../assets/images/weapon/ak.png'),
    username: 'LoginUserName',
    rarity: 'epic'
  },
  {
    id: 12,
    itemName: 'AK-47',
    skinName: 'Vulcan',
    isSt: false,
    image: require('../../../assets/images/weapon/ak1.png'),
    username: 'Sereja',
    rarity: 'rare'
  },
  {
    id: 13,
    itemName: 'M4-16',
    skinName: 'Momentum',
    isSt: true,
    image: require('../../../assets/images/weapon/m416.png'),
    username: 'CS go guy',
    rarity: 'regular'
  }
];


export default lastOpenedCasePrices;