import { lastOpenedCasePrices } from './data'

export default class DummyDataService {
  getCaseItems() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(lastOpenedCasePrices);
      }, 500);

    });
  }
}
