import app from '../app';
import { Controls } from '../app/LayerControls';
import EventBus from '../modules/EventBus';

// –– Navigation ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
// app.$navigation.navigate = NavigationService.navigate;
// app.$navigation.navigateBack = NavigationService.navigateBack;
// app.$navigation.replace = NavigationService.replace;
// app.$navigation.pushToTop = NavigationService.pushToTop;
// app.$navigation.on = NavigationService.on;
// app.$navigation.off = NavigationService.off;
// app.$navigation.EventTypes = NavigationService.EventTypes;

// –– Interaction ––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
const interactionEventBus = new EventBus('App:Interaction', Object.values(app.$interaction.EventTypes));

app.$interaction.subscribe = interactionEventBus.on.bind(interactionEventBus);
app.$interaction.unsubscribe = interactionEventBus.off.bind(interactionEventBus);

app.$interaction.Controls = Controls;

app.$interaction.showControl = interactionEventBus
  .emit.bind(interactionEventBus, app.$interaction.EventTypes.SHOW_CONTROL);

export default app;
