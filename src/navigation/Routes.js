export const Routes = {
  INDEX: 'BottomNavigationRoot',
  Auth: {
    Login: 'auth:login',
  },

  User: {
    PROFILE: 'user:profile',
    SETTINGS: 'user:settings',
    PROMOCODE: 'user:promocode',
  },
  Case: {
    INDEX: 'case:index',
    OPEN: 'case:open',
  },
  Tabs: {
    PROFILE: 'tabs:profile:index',
    CASE: 'tabs:case:index',
  }
};