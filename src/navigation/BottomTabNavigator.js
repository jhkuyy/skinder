import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Profile, Case } from './NavigationStack';
import { Icon } from '../components/base';
import Icons from '../components/base/Icon/icons';
import { Colors } from '../theme';

const Tabs = createBottomTabNavigator({
    Case: {
      screen: Case,
      navigationOptions: {
        tabBarIcon: () => <Icon image={Icons.star} />
      }
    },
    Profile: {
        screen: Profile,
        navigationOptions: {
            tabBarIcon: () => <Icon image={Icons.user} />
        }
    }
}, {
  tabBarOptions: {
    showLabel: false,
    style: {
      backgroundColor: Colors.Background.DARK,
      borderTopColor: Colors.Background.DARK,
    },
  },
});

export default createStackNavigator({ Tabs }, {
  headerMode: 'none',
  cardStyle: {
    backgroundColor: Colors.Background.DARK,
  }
});