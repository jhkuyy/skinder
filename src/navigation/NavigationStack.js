import { Routes } from './Routes';
import StackNavigator from './StackNavigator';

import {
  ProfileScreen,
  PromocodeScreen,
  SettingsScreen,
  CaseScreen,
  CaseOpenScreen
} from '../components/screen';

const Profile =  StackNavigator({
  [Routes.User.PROFILE]: ProfileScreen,
  [Routes.User.PROMOCODE]: PromocodeScreen,
  [Routes.User.SETTINGS]: SettingsScreen,
});

const Case =  StackNavigator({
  [Routes.Case.INDEX]: { screen: CaseScreen },
  [Routes.Case.OPEN]: CaseOpenScreen,
});

export {
  Profile,
  Case,
}