import React from 'react';
import { createAppContainer } from 'react-navigation';
import BottomTabNavigator from './BottomTabNavigator';
import StackNavigator from './StackNavigator';
import {Routes} from './Routes';
import {LoginScreen} from '../components/screen';

const Navigator = StackNavigator({
  [Routes.Auth.Login]: LoginScreen,
  [Routes.INDEX]: { screen: BottomTabNavigator },
});

const AppContainer = createAppContainer(Navigator);

export default class Router extends React.PureComponent {
  render() {
    return (
      <AppContainer />
    );
  }
}
