import { createStackNavigator } from 'react-navigation-stack';
import { Colors } from '../theme';


export default screens => createStackNavigator(screens, {
  cardStyle: { backgroundColor: Colors.Background.DARK },
  headerMode: 'none',
});
